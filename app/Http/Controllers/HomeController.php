<?php

namespace App\Http\Controllers;

use App\Services\Builder\Director;
use App\Services\Builder\HomeBuilder;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function show(Request $request)
    {
        return response()->json((new Director(new HomeBuilder()))
            ->getProduct($request->all()), 200);
    }
}
