<?php
/**
 * Created by PhpStorm.
 * User: mihail
 * Date: 10/16/19
 * Time: 11:03 AM
 */

namespace App\Http\Controllers;


use App\Services\FactoryMethod\PostFactory;
use Illuminate\Http\Request;

class VkPublishController extends Controller
{
    /**
     * @param Request $request
     * @throws \Illuminate\Validation\ValidationException
     */
    public function publish(Request $request)
    {
        $this->validate($request, [
            'owner_id' => 'required',
            'message'  => 'required'
        ]);

        $post = PostFactory::getPost('vk', $request->input('owner_id'), $request->input('message'));
        $result = PostFactory::getGroup('vk')->publish($post);
        $result = $result ? 'publish success' : 'publish error';
        return response()->json(['message' => $result], 200);
    }
}