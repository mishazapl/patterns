<?php

namespace App\Http\Controllers;

use App\Services\Builder\CarBuilder;
use App\Services\Builder\Director;
use Illuminate\Http\Request;

class CarController extends Controller
{
    public function show(Request $request)
    {
        return response()->json((new Director(new CarBuilder()))
            ->getProduct($request->all()), 200);
    }
}
