<?php
/**
 * Created by PhpStorm.
 * User: mihail
 * Date: 10/15/19
 * Time: 2:04 PM
 */

namespace App\Services\Interfaces;


interface ToArray
{
    /**
     * @return array
     */
    public function toArray(): array;
}