<?php
/**
 * Created by PhpStorm.
 * User: mihail
 * Date: 9/23/19
 * Time: 5:51 PM
 */

namespace App\Services\AbstractFactory;


class Player implements Role
{
    public function getTitle(): string
    {
        return 'Player';
    }
}