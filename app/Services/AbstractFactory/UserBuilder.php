<?php
/**
 * Created by PhpStorm.
 * User: mihail
 * Date: 9/23/19
 * Time: 5:26 PM
 */

namespace App\Services\AbstractFactory;


abstract class UserBuilder
{
    protected $type;

    public function __construct(int $type)
    {
        $this->type = $type;
    }

    abstract public function role(RoleFactory $factory): Role;

    abstract public function guns(GunsFactory $factory): array;

    abstract public function build(UserBuilder $user): UserBuilder;
}