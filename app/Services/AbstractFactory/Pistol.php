<?php
/**
 * Created by PhpStorm.
 * User: mihail
 * Date: 9/23/19
 * Time: 5:55 PM
 */

namespace App\Services\AbstractFactory;


class Pistol implements Guns
{

    public function getTitle()
    {
        return 'Pistol';
    }
}