<?php
/**
 * Created by PhpStorm.
 * User: mihail
 * Date: 9/23/19
 * Time: 5:38 PM
 */

namespace App\Services\AbstractFactory;


class AdminGunFactory implements GunsFactory
{

    public function firstGun()
    {
        return new Pistol();
    }

    public function twoGun()
    {
        return new ShotGun();
    }

    public function getGuns()
    {
        return [
            $this->firstGun(),
            $this->twoGun()
        ];
    }
}