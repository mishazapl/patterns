<?php
/**
 * Created by PhpStorm.
 * User: mihail
 * Date: 9/23/19
 * Time: 5:33 PM
 */

namespace App\Services\AbstractFactory;


interface Guns
{
    public function getTitle();
}