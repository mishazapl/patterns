<?php
/**
 * Created by PhpStorm.
 * User: mihail
 * Date: 9/23/19
 * Time: 5:38 PM
 */

namespace App\Services\AbstractFactory;


class PlayerGunFactory implements GunsFactory
{

    public function firstGun()
    {
        return new Pistol();
    }

    public function getGuns()
    {
        return [
            $this->firstGun()
        ];
    }
}