<?php
/**
 * Created by PhpStorm.
 * User: mihail
 * Date: 9/23/19
 * Time: 5:37 PM
 */

namespace App\Services\AbstractFactory;


class PlayerFactory implements RoleFactory
{

    public function getRole()
    {
        return new Player();
    }
}