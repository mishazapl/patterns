<?php
/**
 * Created by PhpStorm.
 * User: mihail
 * Date: 9/23/19
 * Time: 5:31 PM
 */

namespace App\Services\AbstractFactory;


class User extends UserBuilder
{
    /**
     * @var array
     */
    private $factories = [
        0 => [PlayerFactory::class, PlayerGunFactory::class],
        1 => [AdminFactory::class, AdminGunFactory::class]
    ];

    public $role;
    public $guns;

    public function role(RoleFactory $factory): Role
    {
        return $factory->getRole();
    }

    public function guns(GunsFactory $factory): array
    {
        return $factory->getGuns();
    }

    public function build(UserBuilder $user): UserBuilder
    {
        $factories = $this->factories[$user->type];
        $this->role = $this->role(new $factories[0]);
        $this->guns = $this->guns(new $factories[1]);

        return $this;
    }
}