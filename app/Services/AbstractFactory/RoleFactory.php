<?php
/**
 * Created by PhpStorm.
 * User: mihail
 * Date: 9/23/19
 * Time: 5:29 PM
 */

namespace App\Services\AbstractFactory;


interface RoleFactory
{
    public function getRole();
}