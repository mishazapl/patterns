<?php
/**
 * Created by PhpStorm.
 * User: mihail
 * Date: 10/14/19
 * Time: 6:20 PM
 */

namespace App\Services\Builder;


class HomeProduct
{
    public $windows;
    public $garage;
    public $dogs;

    /**
     * @param mixed $windows
     * @return HomeProduct
     */
    public function setWindows($windows)
    {
        $this->windows = $windows;
        return $this;
    }

    /**
     * @param mixed $garage
     * @return HomeProduct
     */
    public function setGarage($garage)
    {
        $this->garage = $garage;
        return $this;
    }

    /**
     * @param mixed $dogs
     * @return HomeProduct
     */
    public function setDogs($dogs)
    {
        $this->dogs = $dogs;
        return $this;
    }
}