<?php
/**
 * Created by PhpStorm.
 * User: mihail
 * Date: 10/14/19
 * Time: 6:11 PM
 */

namespace App\Services\Builder;


interface Builder
{
    public function make(array $parameters = []): Builder;

    public function getProduct();
}