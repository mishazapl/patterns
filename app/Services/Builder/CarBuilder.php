<?php
/**
 * Created by PhpStorm.
 * User: mihail
 * Date: 10/14/19
 * Time: 6:15 PM
 */

namespace App\Services\Builder;


class CarBuilder implements Builder
{
    private $car;

    public function make(array $parameters = []): Builder
    {
        $default = ['wheels' => 4, 'windows' => 4];
        $carProduct = new CarProduct();
        $carProduct->setWheels($parameters['wheels'] ?? $default['wheels']);
        $carProduct->setWindows($parameters['windows'] ?? $default['windows']);
        $this->car = $carProduct;

        return $this;
    }

    public function getProduct()
    {
        return $this->car;
    }
}