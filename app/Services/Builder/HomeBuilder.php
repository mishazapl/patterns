<?php
/**
 * Created by PhpStorm.
 * User: mihail
 * Date: 10/14/19
 * Time: 6:20 PM
 */

namespace App\Services\Builder;


class HomeBuilder implements Builder
{
    private $home;

    public function make(array $parameters = []): Builder
    {
        $default = ['windows' => 4, 'garage' => 0, 'dogs' => 0];
        $homeProduct = new HomeProduct();
        $homeProduct->setWindows($parameters['windows'] ?? $default['windows'])
            ->setGarage($parameters['garage'] ?? $default['garage'])
            ->setDogs($parameters['dogs'] ?? $default['dogs']);

        $this->home = $homeProduct;

        return $this;
    }

    public function getProduct()
    {
        return $this->home;
    }
}