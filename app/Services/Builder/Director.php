<?php
/**
 * Created by PhpStorm.
 * User: mihail
 * Date: 10/14/19
 * Time: 6:12 PM
 */

namespace App\Services\Builder;


class Director
{
    /**
     * @var Builder
     */
    private $builder;

    /**
     * Director constructor.
     * @param Builder $builder
     */
    public function __construct(Builder $builder)
    {
        $this->builder = $builder;
    }

    /**
     * @return Builder
     */
    public function getBuilder(): Builder
    {
        return $this->builder;
    }

    /**
     * @param Builder $builder
     */
    public function setBuilder(Builder $builder): void
    {
        $this->builder = $builder;
    }

    /**
     * @param array $parameters
     * @return mixed
     */
    public function getProduct(array $parameters = [])
    {
        return $this->builder->make($parameters)->getProduct();
    }
}