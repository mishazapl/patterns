<?php
/**
 * Created by PhpStorm.
 * User: mihail
 * Date: 10/14/19
 * Time: 6:16 PM
 */

namespace App\Services\Builder;


class CarProduct
{
    public $wheels;
    public $windows;

    /**
     * @param mixed $wheels
     * @return CarProduct
     */
    public function setWheels($wheels)
    {
        $this->wheels = $wheels;
        return $this;
    }

    /**
     * @param mixed $windows
     */
    public function setWindows($windows): void
    {
        $this->windows = $windows;
    }
}