<?php
/**
 * Created by PhpStorm.
 * User: mihail
 * Date: 10/15/19
 * Time: 2:18 PM
 */

namespace App\Services\FactoryMethod;


use App\Services\FactoryMethod\Vk\Group;
use App\Services\FactoryMethod\Vk\Post;
use App\Services\Interfaces\ToArray;

class PostFactory
{
    private static $configuration  = [
        'vk' => [
            'group' => Group::class,
            'post'  => Post::class
        ]
    ];

    /**
     * @param string $type
     * @return PostPublish
     */
    public static function getGroup(string $type = 'vk'): PostPublish
    {
        return new self::$configuration[$type]['group']() ?? abort(501);
    }

    /**
     * @param string $type
     * @param array $params
     * @return ToArray
     */
    public static function getPost(string $type = 'vk', ...$params): ToArray
    {
        return new self::$configuration[$type]['post'](...$params) ?? abort(501);
    }
}