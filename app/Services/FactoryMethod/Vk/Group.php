<?php
/**
 * Created by PhpStorm.
 * User: mihail
 * Date: 10/15/19
 * Time: 1:51 PM
 */

namespace App\Services\FactoryMethod\Vk;


use App\Services\FactoryMethod\PostPublish;
use App\Services\Interfaces\ToArray;

class Group implements PostPublish
{

    /**
     * @param ToArray $data
     * @return bool
     */
    public function publish(ToArray $data): bool
    {
        $sendData = $data->toArray();
        // in the full version it is will be send to VK API for example
        return true;
    }
}