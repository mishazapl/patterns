<?php
/**
 * Created by PhpStorm.
 * User: mihail
 * Date: 10/15/19
 * Time: 1:54 PM
 */

namespace App\Services\FactoryMethod\Vk;

use App\Services\Interfaces\ToArray;

class Post implements ToArray
{
    private $ownerId;
    private $friendsOnly = 0;
    private $fromGroup = 1;
    private $message;
    private $signed = 0;

    public function __construct($ownerId, $message)
    {
        $this->ownerId = $ownerId;
        $this->message = $message;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'owner_id'      => $this->ownerId,
            'friends_only'  => $this->friendsOnly,
            'from_group'    => $this->fromGroup,
            'message'       => $this->message,
            'signed'        => $this->signed
        ];
    }

    /**
     * @return mixed
     */
    public function getOwnerId()
    {
        return $this->ownerId;
    }

    /**
     * @param mixed $ownerId
     */
    public function setOwnerId($ownerId): void
    {
        $this->ownerId = $ownerId;
    }

    /**
     * @return int
     */
    public function getFriendsOnly(): int
    {
        return $this->friendsOnly;
    }

    /**
     * @param int $friendsOnly
     */
    public function setFriendsOnly(int $friendsOnly): void
    {
        $this->friendsOnly = $friendsOnly;
    }

    /**
     * @return int
     */
    public function getFromGroup(): int
    {
        return $this->fromGroup;
    }

    /**
     * @param int $fromGroup
     */
    public function setFromGroup(int $fromGroup): void
    {
        $this->fromGroup = $fromGroup;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message): void
    {
        $this->message = $message;
    }

    /**
     * @return int
     */
    public function getSigned(): int
    {
        return $this->signed;
    }

    /**
     * @param int $signed
     */
    public function setSigned(int $signed): void
    {
        $this->signed = $signed;
    }
}