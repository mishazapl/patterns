<?php
/**
 * Created by PhpStorm.
 * User: mihail
 * Date: 10/15/19
 * Time: 1:50 PM
 */

namespace App\Services\FactoryMethod;


use App\Services\Interfaces\ToArray;

interface PostPublish
{
    /**
     * @param ToArray $data
     * @return bool
     */
    public function publish(ToArray $data): bool;
}