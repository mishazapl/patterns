<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class VkPublishTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testVkPublish()
    {
        $response = $this->post('/vk/publish', [
            'owner_id' => 1,
            'message'  => 'Start our company'
        ]);

        $response->assertStatus(200);
    }
}
