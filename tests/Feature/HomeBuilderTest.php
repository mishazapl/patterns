<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class HomeBuilderTest extends TestCase
{
    public function testHomeBuilder()
    {
        $response = $this->post('/home');

        $response->assertStatus(200);
    }
}
