<?php

namespace Tests\Unit;

use App\Services\Builder\Director;
use App\Services\Builder\HomeBuilder;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class HomeBuilderTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testExample()
    {
        $director = new Director(new HomeBuilder());
        $home = $director->getProduct(['windows' => 2]);
        $this->assertTrue($home->windows == 2 && $home->garage == 0 && $home->dogs == 0);

        $director = new Director(new HomeBuilder());
        $home = $director->getProduct(['garage' => 2]);
        $this->assertTrue( $home->garage == 2 && $home->windows == 4 && $home->dogs == 0);

        $director = new Director(new HomeBuilder());
        $home = $director->getProduct();
        $this->assertTrue( $home->windows == 4 && $home->garage == 0 && $home->dogs == 0);
    }
}
