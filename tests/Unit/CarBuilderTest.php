<?php

namespace Tests\Unit;

use App\Services\Builder\CarBuilder;
use App\Services\Builder\Director;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CarBuilderTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testExample()
    {
        $director = new Director(new CarBuilder());
        $car = $director->getProduct(['windows' => 2]);
        $this->assertTrue($car->windows == 2);

        $director = new Director(new CarBuilder());
        $car = $director->getProduct(['wheels' => 1]);
        $this->assertTrue( $car->wheels == 1 && $car->windows == 4);

        $director = new Director(new CarBuilder());
        $car = $director->getProduct();
        $this->assertTrue( $car->wheels == 4 && $car->windows == 4);


    }
}
