<?php

namespace Tests\Unit;

use App\Services\AbstractFactory\Role;
use App\Services\AbstractFactory\User;
use App\Services\AbstractFactory\UserBuilder;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{
    public function provideUserBuilder()
    {
        return [
            [new User(0)],
            [new User(1)],
        ];
    }

    /**
     * @dataProvider provideUserBuilder
     *
     * @param User $user
     */
    public function testUserBuilder(User $user)
    {
        $user = $user->build($user);
        $this->assertIsArray($user->guns);
        $this->assertInstanceOf(Role::class, $user->role);
    }
}
