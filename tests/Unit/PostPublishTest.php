<?php

namespace Tests\Unit;

use App\Services\FactoryMethod\PostFactory;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PostPublishTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testPostPublish()
    {
        $post = PostFactory::getPost('vk', 1, 'It is just test');
        $result = PostFactory::getGroup('vk')->publish($post);

        $this->assertTrue($result);
    }
}
